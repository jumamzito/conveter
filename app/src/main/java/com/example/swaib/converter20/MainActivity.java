package com.example.swaib.converter20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
        EditText value;
        Button convert;
        RadioGroup radioGroups;
        RadioButton radioButton1;
        RadioButton radioButton2;
        TextView showResult,showResultLabel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        value = (EditText)findViewById(R.id.value);
        convert = (Button) findViewById(R.id.convert);
        radioGroups = (RadioGroup) findViewById(R.id.radioGrouops);
        radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
        radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
        showResult = (TextView) findViewById(R.id.showResult);
        showResultLabel = (TextView) findViewById(R.id.showResultLabel);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = value.getText().toString();
                if(input.equals("")){
                    input = "0.00";
                }
                double valueinput =Double.parseDouble(input);

                boolean isvalidinput = isValid(input);
                if(isvalidinput){
                    int radiobuttonid = radioGroups.getCheckedRadioButtonId();
                    radioButton1 = (RadioButton) findViewById(radiobuttonid);
                    if (radioButton1.getText().toString().equals("Celcius to fahrenheit")){
                        double result=((valueinput*9)/5)+32;
                        showResult.setText(String.valueOf(result));

                        showResultLabel .setText("degrees fahrenheit");
                    }
                    else{
                        double result=((valueinput-32)*5)/9;
                        showResult.setText(String.valueOf(result));

                        showResultLabel.setText("degrees celcius");
                    }



                }else{
                    //Snackbar.make(v, "please enter a valid input",Snackbar.LENGTH_LONG).show();
                }
            }
        });

    }
    public boolean isValid(String input){
        boolean isValid = true;
        if(input.trim().equals("")){
            isValid = false;



        }
        return isValid;
    }
}
